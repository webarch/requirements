# Webarchitects Ansible Requirements Role

An Ansible role to update the `requirements.yml` file, replacing `version: latest` with the version string from Ansible Galaxy, GitHub and GitLab.

Currently Ansible Galaxy support needs adding, see [Jeff Geerling's Ansible Requirements Updater](https://github.com/geerlingguy/ansible-requirements-updater) for how this could be implemented.

Features that might be added in the future:

* Update the roles
* Generate a playbook containing all the roles
* Add collection support

## Usage

Add the `localhost` to you inventory, for example when using YAML for the inventory:

```yaml
---
all:
  children:
    localhosts:
      hosts:
        localhost:
...
```

Create a `req.yml` playbook containing a list of roles you want in the `requirements.yml` file:

```yaml
---
- name: Update the requirements.yml file
  hosts:
    - localhosts
  connection: local
  become: false
  vars:
    requirements: true
    requirements_roles:
      - name: requirements
        src: https://git.coop/webarch/requirements.git
        version: latest
        scm: git
        type: gitlab
      - name: yq
        src: https://git.coop/webarch/yq.git
        version: latest
        scm: git
        type: gitlab
  roles:
    - requirements
...
```

Run the playbook in check mode to test it:

```bash
ansible-playbook req.yml -C --diff
```

Run the playbook to write the `requirements.yml` file:

```bash
ansible-playbook req.yml --diff
```

## Role variables

See the [defaults/main.yml](role default variables), these are:

### requirements

Set `requirements` to `true` for the tasks in this role to be run, `requirements` defaults to `false`.

### requirements_path

The path to the `requirements.yml` file, `requirements_path` defaults to `requirements.yml` in the current working directory.

### requirements_roles

A list of roles in the same format as Ansible uses with the addition of a `type` variable, for example:

```yaml
requirements_roles:
  - name: acmesh
    src: https://git.coop/webarch/acmesh.git
    version: latest
    scm: git
    type: gitlab
  - name: alternatives
    src: https://git.coop/webarch/alternatives.git
    version: latest
    scm: git
    type: gitlab
```

#### name

The name of the role, this role requires that the name is specified, it is used for the name of the directory that the role is downloaded to.

#### src

The source of the role. Use the format `namespace.role_name`, if downloading from Galaxy; otherwise, provide a URL pointing to a repository within a git based SCM.

Currently this role only supports `https://` URLs support for `git+file:///home/example/foo` URLs and `git+ssh` URLs like `git@gitlab.company.com:mygroup/ansible-core.git`, might be added in the future.

#### type

The type of src, used for working out the latest version, there are three options for type:

* `galaxy` currently unsupported.
* `github` use the `src` URL to generate a URL like `https://github.com/org/repo/releases/latest` to get a HTTP redirect to the latest version of a role.
* `gitlab` use the `src`URL to generate a URL like `https://gitlab.com/org/repo/releases/-/releases/permalink/latest` to get a HTTP redirect the latest version of a role.

#### requirements_test

When `requirements_test` is `true` create a `TMPDIR` and test downloading all the roles into it, this also work in check mode, `requirements_test` defaults to true.

#### requirements_verify

Use the [argument spec](meta/argument_specs.yml) to verify all variables that start with `requirements_`.

## Notes

The format of the `requirements.yml` file is [specified here](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#installing-multiple-roles-from-a-file).

## Copyright

Copyright 2023-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
